import React, { useEffect ,useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import InputForm from "./components/InputForm";
import {Divider} from 'antd'
import StudentList from "./components/StudentList";
import axios from 'axios'

function App() {
  const [students,setStudents] = useState({})
  const [student,setStudent] = useState('')
  const [formData,setFormData] = useState({
    name : '',
    surname : '',
    major : '',
    gpa : 0 ,
  })

  const [name,setName] = useState('')
  const [surname,setSurname] = useState('')
  const [major,setMajor] = useState('')
  const [gpa,setGpa] = useState(0)

  useEffect(()=>{
    getStudents()
  },[])
  const getStudents = async () => {
    const result = await axios.get(`http://localhost/api/students`)
    setStudents(result.data)
  }

  const getStudent = async (id) => {
    const result = await axios.get(`http://localhost/api/students/${id}`)
    console.log('student id : ',result.data)
    setStudent(result.data)
  }


  const addStudent = async (name, surname, major ,gpa) => {
    const result = await axios.post(`http://localhost/api/students`,{
      name,surname,major,gpa
    })
    console.log(result.data)
    getStudents()
    
  }
  const deleteStudent = async (id) => {
    const result = await axios.delete(`http://localhost/api/students/${id}`)
    console.log(result.data)
    getStudents()
  }

  const updateStudent = async (id) => {
    const result = await axios.put(`http://localhost/api/students/${id}`)
    console.log(result.data)
    getStudents()
  }

  return (
    <div>
      <p id="header"><h1>Redux CRUD</h1></p>
      <div className="container">
        <InputForm formData={formData} addStudent={addStudent} onChange={setFormData} />
        <Divider/>
      </div>
      <div className="flex-list">
        <StudentList students={students} deleteStudent={deleteStudent} updateStudent={updateStudent} />
      </div>
    </div>
  );
}

export default App;
